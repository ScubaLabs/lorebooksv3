﻿using UnityEngine;


public class TapToPlaceParent : MonoBehaviour
{
    bool placing = false;
    public GameObject bookCover;
    public UnityEngine.XR.WSA.Input.GestureRecognizer MoveRecognizer { get; private set; }
    
    void Start()
    {
        MoveRecognizer = new UnityEngine.XR.WSA.Input.GestureRecognizer();
        MoveRecognizer.TappedEvent += (source, tapCount, ray) =>
        {
            if (placing)
            {
                placing = false;
                MoveRecognizer.StopCapturingGestures();
                SpatialMapping.Instance.DrawVisualMeshes = false;
            }
        };
    }

    
    void OnSelect()
    {
        placing = true;

        if (placing)
        {
            SpatialMapping.Instance.DrawVisualMeshes = true;
            MoveRecognizer.StartCapturingGestures();

        } else {

            SpatialMapping.Instance.DrawVisualMeshes = false;
            MoveRecognizer.StopCapturingGestures();
        }

    }


    void Update()
    {
        if (placing)
        {

            var headPosition = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;
            RaycastHit hitInfo;

            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo,
                30.0f, SpatialMapping.PhysicsRaycastMask)) {
                var pt = hitInfo.point;
                pt.z = bookCover.transform.position.z;
                bookCover.transform.position = hitInfo.point;
                Quaternion toQuat = Camera.main.transform.localRotation;
                toQuat.x = 0;
                toQuat.z = 0;
                bookCover.transform.rotation = toQuat;
                bookCover.transform.Rotate(0, 270, 75);
            }
        }
     }
}