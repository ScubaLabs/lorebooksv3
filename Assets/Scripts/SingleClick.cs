﻿using UnityEngine;

public class SingleClick : GlowIcon {

    #region Script's Summary
    //This script is checking the time between clciks
    #endregion

    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    private float coolDownTimer = 0.2f;
    private float currentTimer = 0;

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    protected void Update()
    {
        if (currentTimer > 0)
            currentTimer -= Time.deltaTime;
    }

    /// <summary>
    /// click will only work in a fixed timeframe
    /// </summary>
    /// <returns></returns>
    protected bool CanClick()
    {
        if (currentTimer <= 0)
        {
            currentTimer = coolDownTimer;
            return true;
        }
        return false;
    }

    //----------------------------------------------------------------------

    #endregion
}
