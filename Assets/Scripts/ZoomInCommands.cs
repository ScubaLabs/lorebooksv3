﻿using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class ZoomInCommands : MonoBehaviour, IInputClickHandler
{

    public GameObject coverObject;
    public GameObject actionBarObject;
    private float targetZoomInMax = 0.3F;
    //private double targetZoomInMin = 0.1F;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("Zoom in button OnSelect called");
        Debug.Log(coverObject.transform.localScale);
        Debug.Log(coverObject.transform.localPosition);
        Debug.Log(coverObject.transform.localScale.x);
        Debug.Log(coverObject.transform.localScale.x + " >> " + targetZoomInMax);

        if (coverObject.transform.localScale.x < targetZoomInMax)
            coverObject.transform.localScale += new Vector3(0.1F / 4f, 0.1F / 4f, 0.1F / 4f);
    }

    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        Debug.Log("Zoom in button OnSelect called");
        Debug.Log(coverObject.transform.localScale);
        Debug.Log(coverObject.transform.localPosition);
        Debug.Log(coverObject.transform.localScale.x);
        Debug.Log(coverObject.transform.localScale.x + " >> " + targetZoomInMax);

        if (coverObject.transform.localScale.x< targetZoomInMax)
        {
            coverObject.transform.localScale += new Vector3(0.1F / 4f, 0.1F / 4f, 0.1F / 4f);
            //actionBarObject.transform.localScale -= new Vector3(0.1F, 0.1F, 0.1F);
            //actionBarObject.transform.localPosition -= new Vector3(0.1F, 0.1F, 0.1F);

        } 
    }

}
