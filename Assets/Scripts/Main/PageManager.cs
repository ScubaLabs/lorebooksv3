﻿using TMPro;
using UnityEngine;
using UnityEngine.Video;
using LitJson;
using System.Collections.Generic;

public class PageManager : MonoBehaviour
{

    #region Script's Summary
    //This script is managing the pages and operation on them
    #endregion

    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public TextMeshProUGUI pageTextMeshProLeft;
    public TextMeshProUGUI pageTextMeshProRight;
    public VideoPlayer video;
    public GameObject spawnedObject;

    public int getCurrentPage
    {
        get { return currentPage; }
    }

    private int currentPage = 2;
    private string bookName;
    private JsonData imageContent;

    private Dictionary<string, string> pageForImages;
    private Dictionary<string, string> modelForImages;
    private GameObject[] allModels;
    private VideoClip[] allVideos;

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    void Start()
    {
        //--------- variable definitions ------------

        pageForImages = new Dictionary<string, string>();
        modelForImages = new Dictionary<string, string>();
        allModels = Resources.LoadAll<GameObject>("BookMod");
        allVideos = Resources.LoadAll<VideoClip>("BookVid");

        //-------------------------------------------
    }

    /// <summary>
    /// initialize the content of the pages
    /// called from the other script
    /// </summary>
    /// <param name="text">loaded content</param>
    public void InitBook(string bookName, string pageContent, string imageContent)
    {
        this.bookName = bookName;
        this.imageContent = JsonMapper.ToObject(imageContent);

        pageTextMeshProLeft.gameObject.SetActive(true);
        pageTextMeshProRight.gameObject.SetActive(true);

        pageTextMeshProLeft.text = pageContent;
        pageTextMeshProRight.text = pageContent;

        ProcessImageData();
        ChangePages();
    }

    /// <summary>
    /// change the page count
    /// </summary>
    /// <param name="isNext">is next button clicked</param>
    public void PageFlip(bool isNext)
    {
        if (isNext)
            currentPage += 2;
        else //previous clicked
            currentPage -= 2;

        ChangePages();
    }

    /// <summary>
    /// page is clicked 
    /// </summary>
    public void PageImageClick(PageSide pageSide)
    {
        DisableObjects();

        int currentPage = this.currentPage;
        if(pageSide == PageSide.Left)
            currentPage = this.currentPage - 1;
        
        foreach (KeyValuePair<string, string> page in pageForImages)
        {
            if (page.Value == currentPage.ToString())
            {
                foreach (VideoClip vid in allVideos)
                {
                    if (vid.name == page.Key)
                    {
                        video.clip = vid;
                        video.gameObject.SetActive(true);
                        video.Play();
                    }
                }
            }
        }

        foreach (KeyValuePair<string, string> page in modelForImages)
        {
            if (page.Value == currentPage.ToString())
            {
                foreach (GameObject mod in allModels)
                {
                    if (mod.name == page.Key)
                        Instantiate(mod, spawnedObject.transform);
                }
            }
        }
    }

    /// <summary>
    /// process the image data and extract the pages of image
    /// </summary>
    private void ProcessImageData()
    {
        for (int i = 0; i < imageContent["books"].Count; i++)
        {
            if ((string)imageContent["books"][i]["bookName"] == bookName)
            {
                //store the page numbers in the array

                for (int j = 0; j < imageContent["books"][i]["images"].Count; j++)
                    pageForImages.Add((string)imageContent["books"][i]["images"][j]["id"],
                        (string)imageContent["books"][i]["images"][j]["page"]);

                for (int j = 0; j < imageContent["books"][i]["models"].Count; j++)
                    modelForImages.Add((string)imageContent["books"][i]["models"][j]["id"],
                        (string)imageContent["books"][i]["models"][j]["page"]);
            }
        }
    }

    /// <summary>
    /// change the page content
    /// </summary>
    private void ChangePages()
    {
        if (currentPage > 2)
        {
            pageTextMeshProLeft.pageToDisplay = currentPage - 1;
            pageTextMeshProRight.pageToDisplay = currentPage;
        }
        else
        {
            pageTextMeshProLeft.pageToDisplay = 1;
            pageTextMeshProRight.pageToDisplay = 2;
        }
    }

    /// <summary>
    /// disable the image and model
    /// </summary>
    private void DisableObjects()
    {
        video.gameObject.SetActive(false);

        if(spawnedObject.transform.childCount > 0)
            Destroy(spawnedObject.transform.GetChild(0).gameObject);
    }

    //----------------------------------------------------------------------

    #endregion
}