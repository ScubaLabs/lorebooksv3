﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowIcon: MonoBehaviour{

    private float glowValue = 0.8f;

    public void TurnOnLights(Material mat)
    {
        glowValue = mat.GetFloat("_TransitionAlpha");
        mat.SetFloat("_TransitionAlpha", 1f);
    }

    public void TurnOffLights(Material mat)
    {
        mat.SetFloat("_TransitionAlpha", glowValue);
    }
}
