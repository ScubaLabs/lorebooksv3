﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CloseButton : MonoBehaviour, IInputClickHandler {

    #region Script's Summary
    //This script is 
    #endregion

    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------



    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    void Start()
    {
        //--------- variable definitions ------------



        //-------------------------------------------
    }
    
    public void OnInputClicked(InputClickedEventData eventData)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    //----------------------------------------------------------------------

    #endregion
}