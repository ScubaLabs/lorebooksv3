﻿using UnityEngine;

public class BookController : MonoBehaviour
{
    #region Script's Summary
    //This script is managing the opening and closing of book with page flip animation
    #endregion

    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------
    
    public Animator bookAnimator;
    public GameObject[] allPages;

    public bool isBookOpen
    {
        get { return bookOpenStatus; }
    }

    private bool bookOpenStatus = false;
    private PageManager pageManagerScript;
    
    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    public void Start()
    {
        pageManagerScript = GetComponent<PageManager>();
        
    }

    /// <summary>
    /// open the book
    /// </summary>
    private void OpenBook()
    {
        bookOpenStatus = true;
        bookAnimator.SetBool("IsOpened", bookOpenStatus);
        Invoke("EnableOrDisablePages", 1f);
    }

    /// <summary>
    /// enable or disable pages
    /// </summary>
    private void EnableOrDisablePages()
    {
        foreach (GameObject page in allPages)
            page.SetActive(bookOpenStatus);
    }

    /// <summary>
    /// Turn to next page.
    /// </summary>
	public void nextPage()
    {
        if (!bookOpenStatus)
        {
            OpenBook();
            return;
        }

        allPages[1].GetComponentInChildren<Animator>().Play("pageNext");
        pageManagerScript.PageFlip(true);        
    }

    /// <summary>
    /// Turn to previos page
    /// </summary>
	public void prevPage()
    {
        if (!bookOpenStatus)
            return;

        //close the book
        if (pageManagerScript.getCurrentPage <= 2)
        {
            bookOpenStatus = false;
            bookAnimator.SetBool("IsOpened", bookOpenStatus);
            Invoke("EnableOrDisablePages", 1f);
        }
        else
        {
            allPages[1].GetComponentInChildren<Animator>().Play("pagePrev");
            pageManagerScript.PageFlip(false);
        }
    }

    /// <summary>
    /// Exit the application
    /// </summary>
    public void CloseApplication()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor) // if is user on windows editor
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false; //Will pause the scene
#endif
        }
        else
            Application.Quit();         // When runing the app on device
    }

    //----------------------------------------------------------------------

    #endregion
}
