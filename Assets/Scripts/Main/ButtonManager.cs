﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PageSide
{
    Left,
    Right
}

public class ButtonManager : MonoBehaviour {


    #region Script's Summary
    //This script is managing all the button clicks
    #endregion

    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public BookController bookControllerScript;
    public GameObject coverObject;

    private float targetZoomOutMin = 0.05F;
    private float targetZoomInMax = 0.3F;

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    void Start()
    {
        //--------- variable definitions ------------



        //-------------------------------------------
    }

    /*void Update()
    {

    }*/

    public void ButtonClick(string name)
    {
        switch (name)
        {
            case "RightTurn":
                RightTurn();
            break;

            case "LeftTurn":
                LeftTurn();
            break;

            case "ZoomOut":
                ZoomOut();
             break;

            case "ZoomIn":
                ZoomIn();
            break;

            case "Close":
                Close();
            break;
        }
    }

    
    /// perform a action based on button click.
    
    private void RightTurn()
    {
        bookControllerScript.nextPage();
    }

    private void LeftTurn()
    {
        bookControllerScript.prevPage();
    }

    private void ZoomOut()
    {
        if (coverObject.transform.localScale.x > targetZoomOutMin)
            coverObject.transform.localScale -= new Vector3(0.1F / 4f, 0.1F / 4f, 0.1F / 4f);
    }

    private void ZoomIn()
    {
        if (coverObject.transform.localScale.x < targetZoomInMax)
            coverObject.transform.localScale += new Vector3(0.1F / 4f, 0.1F / 4f, 0.1F / 4f);
    }

    private void Close()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    //----------------------------------------------------------------------

    #endregion
}
