﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CommonButton : SingleClick, IInputClickHandler, IFocusable
{
    private Material currentMaterial;
    private ButtonManager buttonManagerScript;

    void Start()
    {
        currentMaterial = GetComponentInChildren<Renderer>().material;
        buttonManagerScript = GetComponentInParent<ButtonManager>();
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (CanClick())
            buttonManagerScript.ButtonClick(eventData.selectedObject.name);
    }

    public void OnFocusEnter()
    {
        TurnOnLights(currentMaterial);
    }

    public void OnFocusExit()
    {
        TurnOffLights(currentMaterial);
    }
}
