﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour
{
	
    //public GameObject filenamestring;

    /// <summary>
    /// change the cover based on the book selection
    /// </summary>
    public GameObject coverGeo;
    public string ImageDataFileName = "ImageData.txt";

    private string bookName;
    //public Material Skyrim;
    //public Material JungleBook;
    //public Material HarryPotter;

    //private delegate void Callback(string data);

    void Start ()
    {
        if (GameObject.Find("BookManager"))
        {
            //previous scene object
            LoadFile(GameObject.Find("BookManager").tag);
            LoadMaterial(GameObject.Find("BookManager").tag);
        }
        else  //for testing..
        {
            LoadFile();
            LoadMaterial();
        }
    }


    /// <summary>
    /// load the file for the chosen title
    /// </summary>
    /// <param name="bookName">name of the title</param>
    public void LoadFile(string bookName = "Skyrim")
    {
        this.bookName = bookName;
        string filename = bookName + ".txt";
        string filePath = Application.streamingAssetsPath + "/" + filename;
        string filePathForImage = Application.streamingAssetsPath + "/" + ImageDataFileName;

#if UNITY_EDITOR
        filePath = "file://" + filePath;
        filePathForImage = "file://" + filePathForImage;
#endif
        StartCoroutine(FetchFileFromServer(filePath, filePathForImage));
    }

    /// <summary>
    /// load the material for the chosen title
    /// </summary>
    /// <param name="bookname">name of the title to load</param>
    public void LoadMaterial(string bookname = "Skyrim")
    {
        Material[] allMat = Resources.LoadAll<Material>("BookMat");

        //load the material for the selected book
        foreach(Material mat in allMat)
        {
            if(bookname == mat.name)
                coverGeo.GetComponent<Renderer>().material = mat;
        }
    }

    IEnumerator FetchFileFromServer(string bookPath, string imagePath)
    {
		WWW www = new WWW(bookPath);
		yield return www;

        WWW wwwImage = new WWW(imagePath);
        yield return wwwImage;

        if (string.IsNullOrEmpty(www.error))
        {
            GetComponent<PageManager>().InitBook(bookName,www.text, wwwImage.text);
        }
        else
            Debug.Log("file error");
	}
}
