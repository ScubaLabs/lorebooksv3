﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class RotateModel : MonoBehaviour , IManipulationHandler{

    public HandDraggable handDraggableScript;

    public void OnManipulationCanceled(ManipulationEventData eventData)
    {
        handDraggableScript.enabled = true;
    }

    public void OnManipulationCompleted(ManipulationEventData eventData)
    {
        handDraggableScript.enabled = true;
    }

    public void OnManipulationStarted(ManipulationEventData eventData)
    {
        handDraggableScript.enabled = false;
    }

    public void OnManipulationUpdated(ManipulationEventData eventData)
    {
        float multiplier = 1.0f;
        float cameraLocalYRotation = Camera.main.transform.localRotation.eulerAngles.y;

        if (cameraLocalYRotation > 270 || cameraLocalYRotation < 90)
            multiplier = -1.0f;

        if (System.Math.Round(eventData.CumulativeDelta.z, 2) != 0)
            ScaleModel(eventData.CumulativeDelta);
        else
            Rotate(eventData.CumulativeDelta, multiplier); 
    }

    void ScaleModel(Vector3 data)
    {
        var originalScale = (transform.GetChild(0).localScale + Vector3.one*data.z);
        transform.GetChild(0).localScale = originalScale;
    }

    void Rotate(Vector3 data, float multiplier)
    {
        var rotation = new Vector3(data.y * -multiplier, data.x * multiplier);
        transform.GetChild(0).Rotate(rotation * 10f, Space.World);
    }
}
