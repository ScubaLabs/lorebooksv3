﻿using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class PageClick : SingleClick, IInputClickHandler {

    #region Script's Summary
    //This script is managing page clicks
    #endregion

    #region Variable Declaration
    //--------------------Variable Declaration-----------------------------

    public PageManager pageManagerScript;
    public PageSide pageSide;

    //----------------------------------------------------------------------
    #endregion

    #region User's Functions
    //----------------------User's Functions--------------------------------

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if(CanClick())
            pageManagerScript.PageImageClick(pageSide);
    }

    //----------------------------------------------------------------------

    #endregion
}
